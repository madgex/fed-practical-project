# Full Stack dev — practical exercise

We have the design & beginnings of a job board project for the fictional brand PxlPro. We’d like you to take up the reins and progress the project as much as you feel is enough to demonstrate your abilities. As a guide we’d allow 2-3 hours for this exercise, we’re not so much concerned with a ‘finished’ project, more a strong indication as to how you’ve tackled what you aimed to achieve.

## What we're looking for

- A faithful representation of the design using HTML, CSS, potentially some client-side JS if you feel the design/UX would benefit. How much of the design you decide to interpret is up to you.
- On the Node.js backend there's a static model available - run with that, or maybe you'd feel integrating with an API service would be fun?

A guide on what we might ask after:

- That you understood the project setup & were able to run with it.
- Websites should be responsive. The static mockup doesn’t cover that, did you get creative?
- Adoption/usage of the Nunjucks templating language.
- Any server-side integrations.
- Your mark-up.
- Any comments on the accessibility of the document.
- Does the design work for you? If not, what constructive feedback would you give?
- Do you foresee any cross-browser issues with your build?

**Note**: This is an exercise only and not a reflection of how we work at Madgex.

## Prerequisites

You'll need to have [Node v8+](https://nodejs.org/download) installed.

## Installing the server dependencies

Install the required modules to run your local [Hapi.js](https://hapijs.com/) server.

```
npm install
```

## Developing / Running the server

```
npm run dev
```

This will build the initial SCSS files to CSS, setup a watch, browser-sync and start the [Hapi](https://hapijs.com/) server.

## Assets

There are some static assets (fonts/images) already available in `./public`.
The `./public/css` and `./public/js` directories contain built assets and shouldn't be edited directly, you'll need to build out the template(s) and CSS.

- Find `templates` in `./server/templates`
- `CSS` is generated from a build script that compiles any [SCSS](https://sass-lang.com/) files from `./src/scss` to `./public/css`
- `JavaScript` is generated from a build script that copies any files from `./src/js` to `./public/js`. There is a stub file for you to do as you please.

### Browser-sync

To help with authoring, the [browser-sync](https://www.browsersync.io/) tool has been integrated to the dev server to provide automatic updates as you change HTML, CSS, images etc.

## This stack won't show my strengths

If you think your abilities are best shown with an alternative setup, you're welcome to refactor to your hearts content.
