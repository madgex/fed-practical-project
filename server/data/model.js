const pkg = require('../../package.json');

module.exports = {
  appVersion: pkg.version,
  pageTitle: 'PxlPro Jobs',
  navItems: [
    {
      label: 'Home',
      href: '/',
    },
    {
      label: 'Find a job',
      href: '/find-a-job',
    },
    {
      label: 'Careers advice',
      href: '/careers',
    },
  ],
  heroMessage: 'Search 1,570 jobs.',
  // ... add any required global model content here
};
