module.exports = [
  {
    method: 'GET',
    path: '/',
    options: {
      description: 'Homepage',
    },
    handler: function (request, h) {
      return h.view('index', {
        message: 'Hello world',
      });
    },
  },
  {
    method: 'GET',
    path: '/find-a-job',
    handler: function (request, h) {
      return h.view('find-a-job');
    },
  },
  {
    method: 'GET',
    path: '/careers',
    handler: function (request, h) {
      return h.view('careers');
    },
  },
  {
    method: 'GET',
    path: '/public/{param*}',
    options: {
      description: 'Static assets',
    },
    handler: {
      directory: {
        path: '.',
        redirectToSlash: true,
        index: true,
      },
    },
  },
];
